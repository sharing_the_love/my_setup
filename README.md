# my_setup

## encrypt a partition
if this [page](https://www.maketecheasier.com/encrypt-linux-partitions-dm-crypt/) is down, I have made a local [copy](/encrypt_partition/How%20to%20Encrypt%20Your%20Partitions%20on%20Linux%20with%20dm-crypt%20-%20Make%20Tech%20Easier.html)

## bash aliases
- copy bash_aliases to home folder

```
cp .bash_aliases ~
```
- replace 414490624 in ~/.bash_aliases with the starting point of the encrypted drive
- edit bashrc to load bash_aliases:
```
# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
```

## mount and decrypt partition under "Data"
- create Data folder if not already:
```
mkdir -p ~/Data
```
- update bash aliases (see related topic)
- mount and decrypt partition with command:
```
mount_enc
```

## use ssh keys stored under "Data"
```
mkdir -p ~/.ssh
cd ~/.ssh
ln -s ~/Data/ssh_keys/.ssh/id_ed25519
ln -s ~/Data/ssh_keys/.ssh/id_ed25519.pub
```

## configure XFCE
#### install start_or_cycle_through_windows_of
```
sudo pacman -Sy wmctrl
sudo cp start_or_cycle_through_windows_of /usr/bin/
sudo chmod +x /usr/bin/start_or_cycle_through_windows_of
```

#### set shortcuts in the XFCE desktop
```
cp ./xfce/xfce4-keyboard-shortcuts.xml ~/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml
restart
```

#### set border-only theme
- [download](https://www.xfce-look.org/p/1016214/) border-only theme, then:
```
cd ~/Downloads ; tar --extract --file 140039-border-only.tar.tar
echo 'frame_border_top=2' >> ~/Downloads/border-only/xfwm4/themerc
sudo mv border-only /usr/share/themes/
```
- finally pick the theme in "Window Manager" and cleanup:
```
rm ~/Downloads/140039-border-only.tar.tar
```

## configure sway

#### install sway
```
sudo pacman -S sway
```
#### install wopi to search for applications
```
sudo pacman -S wofi
```
#### install foot terminal and configure it
```
sudo pacman -S foot
mkdir -p ~/.config/foot ; cp -rf sway/foot.ini ~/.config/foot/foot.ini
```
#### override default config with my configuration
```
cp -rf sway/my_config ~/.config/sway/config
```
#### install xorg-xwayland in case sway is installed on top of XFCE
```
sudo pamac install xorg-xwayland
```
#### install waybar
```
sudo pacman -S waybar
```
#### override default bar config with my way bar configuration
```
mkdir -p ~/.config/waybar ; cp -rf sway/my_waybar/* ~/.config/waybar/
```
#### install sway-interactive-screenshot
```
pamac install sway-interactive-screenshot
```
#### install swaylock
```
sudo pacman -S swaylock
```
#### install wallpappers changer
```
sudo pacman -S swaybg 
```
## configure i3
#### install i3
preferably through gui with all the default dependencies or with:
```
sudo pacman -S i3-wm i3lock i3status
```
#### install rofi to search for applications
```
sudo pacman -S rofi
```
#### install xrandr to set screens resolutions
```
sudo pacman -S xorg-xrandr
```
#### override default config with my configuration
```
cp -rf i3/my_config ~/.config/i3/config
```
#### enable natural scrolling and tap to click on touchpad
```
sudo nano /usr/share/X11/xorg.conf.d/40-libinput.conf
```
find this
```
Section "InputClass"
        Identifier "libinput touchpad catchall"
        MatchIsTouchpad "on"
        MatchDevicePath "/dev/input/event*"
        Driver "libinput"
EndSection
```
and change it to this
```
Section "InputClass"
        Identifier "libinput touchpad catchall"
        MatchIsTouchpad "on"
        MatchDevicePath "/dev/input/event*"
        Driver "libinput"
        Option "NaturalScrolling" "True"
        Option "Tapping" "on"
EndSection
```
#### configure i3 bar
```
mkdir -p ~/.config/i3status
cp -rf i3/my_i3bar/i3status.conf ~/.config/i3status/config

```
to refresh the bar and see the changes hit
```
$mod+shift+r
```
#### install nitrogen to set wallpapers
```
pamac install nitrogen
```

## configure GNOME 

```
echo "disabling Alt+F6 since it blocks IDE's shortcut..."
gsettings set "org.gnome.desktop.wm.keybindings" "cycle-group" "[]"
echo "disabling Alt+F7 since it blocks IDE's shortcut..."
gsettings set "org.gnome.desktop.wm.keybindings" "begin-move" "[]"
echo "disabling Super+1 since it's blocking custom shortcut"
gsettings set "org.gnome.desktop.wm.keybindings" "switch-to-workspace-1" "[]"
echo "disabling Super+2 since it's blocking custom shortcut"
gsettings set "org.gnome.desktop.wm.keybindings" "switch-to-workspace-2" "[]"
echo "disabling Super+3 since it's blocking custom shortcut"
gsettings set "org.gnome.desktop.wm.keybindings" "switch-to-workspace-3" "[]"
echo "disabling Super+4 since it's blocking custom shortcut"
gsettings set "org.gnome.desktop.wm.keybindings" "switch-to-workspace-4" "[]"
echo 'override "switch to workspace on the left" so as Super+Page_Up can be used elsewhere'
gsettings set "org.gnome.desktop.wm.keybindings" "switch-to-workspace-left" "['<Super><Alt>Left']"
echo 'override "switch to workspace on the right" so as Super+Page_Down can be used elsewhere'
gsettings set "org.gnome.desktop.wm.keybindings" "switch-to-workspace-right" "['<Super><Alt>Right']"
echo "using Super+Page_Up to maximize windows..."
gsettings set "org.gnome.desktop.wm.keybindings" "toggle-maximized" "['<Super>Page_Up']"
echo "using Super+Page_Down to unmaximize windows..."
gsettings set "org.gnome.desktop.wm.keybindings" "unmaximize" "['<Super>Page_Down']"
echo "using Super+H to maximize horizontally..."
gsettings set "org.gnome.desktop.wm.keybindings" "maximize-horizontally" "['<Super>H']"
echo "mod + right click to resize window"
gsettings set org.gnome.desktop.wm.preferences resize-with-right-button true
echo "set KP_1 to start or focus on the 1st app in dash"
gsettings set org.gnome.shell.keybindings switch-to-application-1 "['<Super>KP_End']"
echo "set KP_2 to start or focus on the 2nd app in dash"
gsettings set org.gnome.shell.keybindings switch-to-application-2 "['<Super>KP_2']"
echo "set KP_3 to start or focus on the 3rd app in dash"
gsettings set org.gnome.shell.keybindings switch-to-application-3 "['<Super>KP_3']"
echo "set KP_4 to start or focus on the 4th app in dash"
gsettings set org.gnome.shell.keybindings switch-to-application-4 "['<Super>KP_4']"
echo "set KP_5 to start or focus on the 5th app in dash"
gsettings set org.gnome.shell.keybindings switch-to-application-5 "['<Super>KP_Begin']"
echo "set KP_6 to start or focus on the 6th app in dash"
gsettings set org.gnome.shell.keybindings switch-to-application-6 "['<Super>KP_6']"
echo "set KP_7 to start or focus on the 7th app in dash"
gsettings set org.gnome.shell.keybindings switch-to-application-7 "['<Super>KP_Home']"
echo "set KP_8 to start or focus on the 8th app in dash"
gsettings set org.gnome.shell.keybindings switch-to-application-8 "['<Super>KP_8']"
echo "set KP_9 to start or focus on the 9th app in dash"
gsettings set org.gnome.shell.keybindings switch-to-application-9 "['<Super>KP_9']"
```
