mount_enc () {
    encrypted_partition=$(sudo fdisk -l | grep 414490624 | cut -d " " -f1)
    sudo cryptsetup luksOpen $encrypted_partition encrypted-partition
    sudo mount /dev/mapper/encrypted-partition ~/Data
}
